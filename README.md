# Dark-theme icon-set for Inkscape 0.91

All credits to the original artist [roadsleadme](https://www.deviantart.com/roadsleadme/art/Inkscape-0-91-dark-theme-547919927).

© 2015 - 2019 [roadsleadme](https://www.deviantart.com/roadsleadme)
